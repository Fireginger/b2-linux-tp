# TP2 : Gestion de service

# 0. Prérequis

➜ Machines Rocky Linux

Création d'une machine linux `web.tp2.linux` `10.102.1.11` Serveur Web

# I. Un premier serveur web

## 1. Installation

🌞 **Installer le serveur Apache**

- paquet `httpd`
- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`
```
[thomas@web ~]$ sudo dnf install httpd
Last metadata expiration check: 0:17:12 ago on Tue 15 Nov 2022 02:39:45 PM CET.
Package httpd-2.4.51-7.el9_0.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le
```
[thomas@web ~]$ sudo systemctl start httpd
[thomas@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 14:57:30 CET; 19s ago
       Docs: man:httpd.service(8)
   Main PID: 33219 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5907)
     Memory: 23.0M
        CPU: 62ms
     CGroup: /system.slice/httpd.service
             ├─33219 /usr/sbin/httpd -DFOREGROUND
             ├─33220 /usr/sbin/httpd -DFOREGROUND
             ├─33221 /usr/sbin/httpd -DFOREGROUND
             ├─33222 /usr/sbin/httpd -DFOREGROUND
             └─33223 /usr/sbin/httpd -DFOREGROUND

Nov 15 14:57:30 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 14:57:30 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 14:57:30 web.tp2.linux httpd[33219]: Server configured, listening on: port 80
```
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
```
[thomas@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
  - ouvrez le port firewall nécessaire
```
[thomas@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache
```
[thomas@web ~]$ sudo ss -laputnr
tcp         LISTEN       0            511                                   *:80                           *:*           users:(("httpd",pid=33223,fd=4),("httpd",pid=33222,fd=4),("httpd",pid=33221,fd=4),("httpd",pid=33219,fd=4))
```

# Demander à systemd les logs relatifs au service httpd
```
[thomas@web ~]$ sudo journalctl -xe -u httpd
Nov 15 14:57:30 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
░░ Subject: A start job for unit httpd.service has finished successfully
░░ Defined-By: systemd
░░ Support: https://access.redhat.com/support
░░
░░ A start job for unit httpd.service has finished successfully.
░░
░░ The job identifier is 1661.
Nov 15 14:57:30 web.tp2.linux httpd[33219]: Server configured, listening on: port 80
```

🌞 **TEST**

- vérifier que le service est démarré
```
[thomas@web ~]$ sudo systemctl status httpd
[sudo] password for thomas:
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 14:57:30 CET; 14min ago
       Docs: man:httpd.service(8)
   Main PID: 33219 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5907)
     Memory: 23.0M
        CPU: 493ms
     CGroup: /system.slice/httpd.service
             ├─33219 /usr/sbin/httpd -DFOREGROUND
             ├─33220 /usr/sbin/httpd -DFOREGROUND
             ├─33221 /usr/sbin/httpd -DFOREGROUND
             ├─33222 /usr/sbin/httpd -DFOREGROUND
             └─33223 /usr/sbin/httpd -DFOREGROUND

Nov 15 14:57:30 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 14:57:30 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 14:57:30 web.tp2.linux httpd[33219]: Server configured, listening on: port 80
```
- vérifier qu'il est configuré pour démarrer automatiquement
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
[thomas@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```
[thomas@web ~]$ sudo cat /etc/systemd/system/multi-user.target.wants/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé
```
[thomas@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
User apache
```
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
[thomas@web ~]$ sudo ps -ef | grep httpd
apache     33220   33219  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33221   33219  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33222   33219  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache     33223   33219  0 14:57 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```
- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf
```
[thomas@web testpage]$ ls -al
total 12
drwxr-xr-x.  2 root root   24 Nov 15 14:50 .
drwxr-xr-x. 83 root root 4096 Nov 15 14:50 ..
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```
On peut bien voir que la page index.html est readable par tous les utilisateurs dont Apache.


🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
```
[thomas@web testpage]$ sudo cat /etc/passwd
web:x:1001:1001::/usr/share/httpd:/sbin/nologin
```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
User web
Group web
```

- redémarrez Apache
```
[thomas@web ~]$ sudo systemctl reload httpd
```
- utilisez une commande `ps` pour vérifier que le changement a pris effet
```
[thomas@web ~]$ ps -ef | grep httpd
root         713       1  0 15:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web          739     713  0 15:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web          742     713  0 15:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web          744     713  0 15:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web          745     713  0 15:59 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix
```
ServerRoot "/etc/httpd"

Listen 8888
```

- ouvrez ce nouveau port dans le firewall, et fermez l'ancien
```
[thomas@web ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[thomas@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[thomas@web ~]$ sudo firewall-cmd --reload
success
[thomas@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 8888/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

- redémarrez Apache
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```
tcp         LISTEN       0            511                                   *:8888                        *:*            users:(("httpd",pid=1149,fd=6),("httpd",pid=1148,fd=6),("httpd",pid=1147,fd=6),("httpd",pid=713,fd=6))
```

- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```
[thomas@web ~]$ curl localhost:8888
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
```

- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

📁 **Fichier `/etc/httpd/conf/httpd.conf`**

![httpd](./httpd.conf)

# II. Une stack web plus avancée

## 1. Intro blabla

## 2. Setup

🖥️ **VM db.tp2.linux**

`db.tp2.linux`  `10.102.1.12` Serveur Base de Données 

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
[thomas@db ~]$ dnf install mariadb-server
Complete!
```

```
[thomas@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```
[thomas@db ~]$ sudo systemctl start mariadb
[thomas@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.5 database server
     Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 16:31:18 CET; 7s ago
       Docs: man:mariadbd(8)
             https://mariadb.com/kb/en/library/systemd/
    Process: 3052 ExecStartPre=/usr/libexec/mariadb-check-socket (code=exited, status=0/SUCCESS)
    Process: 3074 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir mariadb.service (code=exited, status=0/SUCCESS)
    Process: 3168 ExecStartPost=/usr/libexec/mariadb-check-upgrade (code=exited, status=0/SUCCESS)
   Main PID: 3155 (mariadbd)
     Status: "Taking your SQL requests now..."
      Tasks: 12 (limit: 5907)
     Memory: 77.6M
        CPU: 239ms
     CGroup: /system.slice/mariadb.service
             └─3155 /usr/libexec/mariadbd --basedir=/usr
```

```
[thomas@db ~]$ sudo mysql_secure_installation

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


Remove anonymous users? [Y/n] y
 ... Success!


Disallow root login remotely? [Y/n] y
 ... Success!


Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`
```
[thomas@db ~]$ sudo ss -laputnr
tcp         LISTEN       0            80                                   *:3306                         *:*            users:(("mariadbd",pid=3155,fd=19))
```

- il sera nécessaire de l'ouvrir dans le firewall
```
[thomas@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[thomas@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 3306/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
  - exécutez les commandes SQL suivantes :

```sql
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```


🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - utilisez la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
```
[thomas@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 19
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]>
```
- une fois connecté à la base, utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.001 sec)
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

- vous ne pourrez pas utiliser l'utilisateur `nextcloud` de la base pour effectuer cette opération : il n'a pas les droits
- il faudra donc vous reconnectez localement à la base en utilisant l'utilisateur `root`

Une fois qu'on s'est assurés qu'on peut se co au service de base de données depuis `web.tp2.linux`, on peut continuer.

```
MariaDB [(none)]> select user,host from mysql.user;
+-------------+-------------+
| User        | Host        |
+-------------+-------------+
| nextcloud   | 10.102.1.11 |
| mariadb.sys | localhost   |
| mysql       | localhost   |
| root        | localhost   |
+-------------+-------------+
4 rows in set (0.001 sec)
```

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```bash
# On ajoute le dépôt CRB
[thomas@web ~]$ sudo dnf config-manager --set-enabled crb

# On ajoute le dépôt REMI
[thomas@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
Complete!

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
[thomas@web ~]$ dnf module list php
Last metadata expiration check: 0:00:05 ago on Tue 15 Nov 2022 05:10:21 PM CET.
Remi's Modular repository for Enterprise Linux 9 - x86_64
Name                Stream                 Profiles                                 Summary
php                 remi-7.4               common [d], devel, minimal               PHP scripting language
php                 remi-8.0               common [d], devel, minimal               PHP scripting language
php                 remi-8.1               common [d], devel, minimal               PHP scripting language
php                 remi-8.2               common [d], devel, minimal               PHP scripting language

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled

# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
[thomas@web ~]$ sudo dnf module enable php:remi-8.1 -y
Complete!

# Eeeet enfin, on installe la bonne version de PHP : 8.1
[thomas@web ~]$ sudo dnf install -y php81-php
Complete!
```

🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
# eeeeet euuuh boom. Là non plus j'ai pas pondu ça, c'est la doc :
[thomas@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp

Installed:
  fontconfig-2.13.94-2.el9.x86_64                            fribidi-1.0.10-6.el9.x86_64
  gd3php-2.3.3-8.el9.remi.x86_64                             gdk-pixbuf2-2.42.6-2.el9.x86_64
  jbigkit-libs-2.1-23.el9.x86_64                             jxl-pixbuf-loader-0.6.1-8.el9.x86_64
  libX11-1.7.0-7.el9.x86_64                                  libX11-common-1.7.0-7.el9.noarch
  libXau-1.0.9-8.el9.x86_64                                  libXpm-3.5.13-7.el9.x86_64
  libaom-3.2.0-4.el9.x86_64                                  libavif-0.10.1-3.el9.x86_64
  libdav1d-0.9.2-1.el9.x86_64                                libicu71-71.1-2.el9.remi.x86_64
  libimagequant-2.17.0-1.el9.x86_64                          libjpeg-turbo-2.0.90-5.el9.x86_64
  libjxl-0.6.1-8.el9.x86_64                                  libraqm-0.8.0-1.el9.x86_64
  libtiff-4.2.0-3.el9.x86_64                                 libvmaf-2.3.0-2.el9.x86_64
  libwebp-1.2.0-3.el9.x86_64                                 libxcb-1.13.1-9.el9.x86_64
  php81-php-bcmath-8.1.12-1.el9.remi.x86_64                  php81-php-gd-8.1.12-1.el9.remi.x86_64
  php81-php-gmp-8.1.12-1.el9.remi.x86_64                     php81-php-intl-8.1.12-1.el9.remi.x86_64
  php81-php-mysqlnd-8.1.12-1.el9.remi.x86_64                 php81-php-pecl-zip-1.21.1-1.el9.remi.x86_64
  php81-php-process-8.1.12-1.el9.remi.x86_64                 remi-libzip-1.9.2-3.el9.remi.x86_64
  shared-mime-info-2.1-4.el9.x86_64                          svt-av1-libs-0.8.7-2.el9.x86_64
  xml-common-0.6.3-58.el9.noarch

Complete!
```

🌞 **Récupérer NextCloud**

- créez le dossier `/var/www/tp2_nextcloud/`
```
[thomas@web www]$ ls
cgi-bin  html  tp2_nextcloud
```
- récupérer le fichier suivant avec une commande `curl` ou `wget` : https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
```
[thomas@web ~]$ wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
--2022-11-15 17:16:11--  https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
Resolving download.nextcloud.com (download.nextcloud.com)... 95.217.64.181, 2a01:4f9:2a:3119::181
Connecting to download.nextcloud.com (download.nextcloud.com)|95.217.64.181|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 176341139 (168M) [application/zip]
Saving to: ‘nextcloud-25.0.0rc3.zip’

nextcloud-25.0.0rc3.zip       100%[=================================================>] 168.17M  4.87MB/s    in 34s

2022-11-15 17:16:46 (4.94 MB/s) - ‘nextcloud-25.0.0rc3.zip’ saved [176341139/176341139]
```

- extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`
  - installez la commande `unzip` si nécessaire
  - vous pouvez extraire puis déplacer ensuite, vous prenez pas la tête
```
[thomas@web ~]$ unzip nextcloud-25.0.0rc3.zip
```

```
[thomas@web ~]$ ls
nextcloud  nextcloud-25.0.0rc3.zip
```

```
[thomas@web ~]$ sudo mv nextcloud /var/www/tp2_nextcloud/
```
  - contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place
```
[thomas@web ~]$ sudo cat /var/www/tp2_nextcloud/nextcloud/index.html
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>
```
- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache
```
[thomas@web tp2_nextcloud]$ ls -al
total 140
drwxr-xr-x. 15 root   root    4096 Nov 15 17:47 .
drwxr-xr-x.  5 root   root      54 Nov 15 17:14 ..
drwxr-xr-x. 47 apache apache  4096 Oct  6 14:47 3rdparty
drwxr-xr-x. 50 apache apache  4096 Oct  6 14:44 apps
-rw-r--r--.  1 apache apache 19327 Oct  6 14:42 AUTHORS
drwxr-xr-x.  2 apache apache    85 Nov 15 17:48 config
-rw-r--r--.  1 apache apache  4095 Oct  6 14:42 console.php
-rw-r--r--.  1 apache apache 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 23 apache apache  4096 Oct  6 14:47 core
-rw-r--r--.  1 apache apache  6317 Oct  6 14:42 cron.php
drwxr-xr-x.  2 apache apache  8192 Oct  6 14:42 dist
-rw-r--r--.  1 apache apache  3253 Oct  6 14:42 .htaccess
-rw-r--r--.  1 apache apache   156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache apache  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache apache   125 Oct  6 14:42 lib
-rw-r--r--.  1 apache apache   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocm-provider
drwxr-xr-x.  2 apache apache    55 Oct  6 14:42 ocs
drwxr-xr-x.  2 apache apache    23 Oct  6 14:42 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache apache  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache apache   133 Oct  6 14:42 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache apache    35 Oct  6 14:42 themes
drwxr-xr-x.  2 apache apache    43 Oct  6 14:44 updater
-rw-r--r--.  1 apache apache   101 Oct  6 14:42 .user.ini
-rw-r--r--.  1 apache apache   387 Oct  6 14:47 version.php
```

🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf
- créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :

```
[thomas@web conf.d]$ cat apache.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf
```
[thomas@web ~]$ sudo systemctl restart httpd
[thomas@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php81-php-fpm.conf
     Active: active (running) since Tue 2022-11-15 17:29:15 CET; 9s ago
       Docs: man:httpd.service(8)
   Main PID: 5049 (httpd)
     Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
      Tasks: 213 (limit: 5905)
     Memory: 23.4M
        CPU: 60ms
     CGroup: /system.slice/httpd.service
             ├─5049 /usr/sbin/httpd -DFOREGROUND
             ├─5050 /usr/sbin/httpd -DFOREGROUND
             ├─5051 /usr/sbin/httpd -DFOREGROUND
             ├─5052 /usr/sbin/httpd -DFOREGROUND
             └─5053 /usr/sbin/httpd -DFOREGROUND

Nov 15 17:29:15 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 17:29:15 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 17:29:15 web.tp2.linux httpd[5049]: Server configured, listening on: port 80
```

### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

```
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.102.1.11	web.tp2.linux
```

- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

![Nextcloud](./Capture.PNG)

🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
```
[thomas@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 307
Server version: 10.5.16-MariaDB MariaDB Server
```

- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL
```
MariaDB [(none)]> USE nextcloud;

Database changed

MariaDB [nextcloud]> show tables;
+-----------------------------+
| Tables_in_nextcloud         |
  Beaucoup de tables 
+-----------------------------+
124 rows in set (0.001 sec)
```

Nextcloud a crée 124 tables dans la database.
