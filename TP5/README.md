# TP5 : Mattermost

Groupe :
- GADEBILLE Baptiste (https://gitlab.com/baptgdb/tp_linux_b2/-/tree/main/TP5_Linux_B2)
- EVEILLARD Thomas

## 0. Prérequis

Il faut préparer deux machines :
- Une machine qui va avoir mattermost sur elle, on va l'appeler 'Mattermost.tp5' avec une IP : "10.104.1.10"
- Une machine qui va avoir la db Mysql sur elle, on va l'appeler 'Database.tp5' avec une IP : "10.104.1.20"

Sur les deux machines on va faire la configuration de bases et s'assurer que la machine est à jour avec :
```
[thomas@mattermost ~]$ sudo dnf update -y
Last metadata expiration check: 4:10:19 ago on Wed 07 Dec 2022 12:37:46 PM CET.
Dependencies resolved.
Nothing to do.
Complete!
```

## 1. Mysql

Sur la database on va venir installer Mysql :

```
[thomas@database ~]$ sudo dnf install mysql-server
Complete!
```

```
[thomas@database ~]$ sudo systemctl start mysqld.service
[thomas@database ~]$ sudo systemctl enable mysqld
[thomas@database ~]$ sudo systemct status mysqld
sudo: systemct: command not found
[thomas@database ~]$ sudo systemctl status mysqld
● mysqld.service - MySQL Server
   Loaded: loaded (/usr/lib/systemd/system/mysqld.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2022-12-07 12:30:07 CET; 4h 49min ago
     Docs: man:mysqld(8)
           http://dev.mysql.com/doc/refman/en/using-systemd.html
  Process: 2020 ExecStartPre=/usr/bin/mysqld_pre_systemd (code=exited, status=0/SUCCESS)
 Main PID: 2048 (mysqld)
   Status: "Server is operational"
    Tasks: 52 (limit: 4908)
   Memory: 339.5M
   CGroup: /system.slice/mysqld.service
           └─2048 /usr/sbin/mysqld
```

```
[thomas@database ~]$ sudo mysql_secure_installation

Securing the MySQL server deployment.

Enter password for user root:
The 'validate_password' component is installed on the server.
The subsequent steps will run with the existing configuration
of the component.
Using existing password for root.

Estimated strength of the password: 100
Change the password for root ? ((Press y|Y for Yes, any other key for No) : n

 ... skipping.
By default, a MySQL installation has an anonymous user,
allowing anyone to log into MySQL without having to have
a user account created for them. This is intended only for
testing, and to make the installation go a bit smoother.
You should remove them before moving into a production
environment.

Remove anonymous users? (Press y|Y for Yes, any other key for No) : y
Success.


Normally, root should only be allowed to connect from
'localhost'. This ensures that someone cannot guess at
the root password from the network.

Disallow root login remotely? (Press y|Y for Yes, any other key for No) : y
Success.

By default, MySQL comes with a database named 'test' that
anyone can access. This is also intended only for testing,
and should be removed before moving into a production
environment.


Remove test database and access to it? (Press y|Y for Yes, any other key for No) : y
 - Dropping test database...
Success.

 - Removing privileges on test database...
Success.

Reloading the privilege tables will ensure that all changes
made so far will take effect immediately.

Reload privilege tables now? (Press y|Y for Yes, any other key for No) : y
Success.

All done!
```

**On peut vérifier que ca marche bien :**
```
[thomas@database ~]$ mysqladmin -u root -p version
Enter password:
mysqladmin  Ver 8.0.31 for Linux on x86_64 (MySQL Community Server - GPL)
Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Server version          8.0.31
Protocol version        10
Connection              Localhost via UNIX socket
UNIX socket             /var/lib/mysql/mysql.sock
Uptime:                 4 hours 52 min 42 sec

Threads: 7  Questions: 16485  Slow queries: 0  Opens: 1385  Flush tables: 3  Open tables: 791  Queries per second avg: 0.938
```

```
[thomas@database ~]$ mysql -u root -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 73
Server version: 8.0.31 MySQL Community Server - GPL

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

**Ensuite on va parametrer la database pour mattermost :**
```
mysql> create user 'mmuser'@'%' identified by 'Thomarikadou4_';

mysql> create database mattermost;

mysql> grant all privileges on mattermost.* to 'mmuser'@'%';
```

**On vérifie bien qu'on a nos databases :**
```
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mattermost         |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.01 sec)
```

## 2. Mattermost

On va pouvoir installer mattermost :
```
[thomas@mattermost ~]$ sudo dnf install wget
Last metadata expiration check: 0:19:28 ago on Thu Dec  1 11:34:13 2022.
Dependencies resolved.
[...]
Installed:
  wget-1.21.1-7.el9.x86_64

Complete!
```

```
[thomas@mattermost ~]$ wget https://releases.mattermost.com/7.5.1/mattermost-7.5.1-linux-amd64.tar.gz
--2022-12-01 11:55:05--  https://releases.mattermost.com/7.5.1/mattermost-7.5.1-linux-amd64.tar.gz
Resolving releases.mattermost.com (releases.mattermost.com)... 13.224.189.7, 13.224.189.43, 13.224.189.78, ...
Connecting to releases.mattermost.com (releases.mattermost.com)|13.224.189.7|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 346059466 (330M) [application/x-tar]
Saving to: ‘mattermost-7.5.1-linux-amd64.tar.gz’

mattermost-7.5.1-linux-am 100%[===================================>] 330.03M  1.97MB/s    in 3m 0s

2022-12-01 11:58:06 (1.83 MB/s) - ‘mattermost-7.5.1-linux-amd64.tar.gz’ saved [346059466/346059466]
```

```
[thomas@mattermost ~]$ tar -xvzf mattermost*.gz
[...]
mattermost/client/main.94bef8661903342eb099.js
mattermost/client/main.94bef8661903342eb099.js.map
mattermost/client/manifest.json
mattermost/client/root.html
mattermost/ENTERPRISE-EDITION-LICENSE.txt
mattermost/NOTICE.txt
mattermost/README.md
mattermost/manifest.txt
```

**Ensuite on donne les permissions pour les différents fichiers de mattermost pour qu'il puisse s'éxécuter.**
```
[thomas@mattermost ~]$ sudo mv mattermost /opt

[thomas@mattermost ~]$ sudo mkdir /opt/mattermost/data

[thomas@mattermost ~]$ sudo useradd --system --user-group mattermost

[thomas@mattermost ~]$ sudo chown -R mattermost:mattermost /opt/mattermost

[thomas@mattermost ~]$ sudo chmod -R g+w /opt/mattermost
```


**Ensuite on vient changer le fichier de conf de mattermost pour relier la base de donnée avec notre serveur :**
```
[thomas@mattermost ~]$ sudo cat /opt/mattermost/config/config.json
"SqlSettings": {
    "DriverName": "mysql",
    "DataSource": "mmuser:Thomarikadou4_@tcp(10.104.1.20:3306)/mattermost?charset=utf8mb4,utf8&writeTimeout=30s",
```

**Enfin on peut lancer mattermost :**
```
[thomas@mattermost mattermost]$ sudo -u mattermost bin/mattermost
[sudo] password for thomas:
{"timestamp":"2022-12-07 21:18:39.954 +01:00","level":"info","msg":"Server is initializing...","caller":"platform/service.go:157","go_version":"go1.18.1"}
{"timestamp":"2022-12-07 21:18:39.956 +01:00","level":"info","msg":"Pinging SQL","caller":"sqlstore/store.go:230","database":"master"}
[...]
{"timestamp":"2022-12-07 21:21:54.866 +01:00","level":"info","msg":"Starting Server...","caller":"app/server.go:913"}
{"timestamp":"2022-12-07 21:21:54.867 +01:00","level":"info","msg":"Server is listening on [::]:8065","caller":"app/server.go:985","address":"[::]:8065"}
```

**Ensuite on se rend sur notre navigateur et on tape notre IP suivi du port donc : "http://10.104.1.10:8065"**
**Et on arrive sur notre mattermost :**

![mattermost](./images/mattermost.PNG)

**Ensuite on va venir créer un utilisateur, sachant que le premier utilisateur crée dispose des droits admin, on va donc pouvoir accéder à la page  de contrôle admin :**

![admin_pannel](./images/admin_pannel.PNG)


Enfin on va créer un service pour mattermost comme ca ce sera plus pratique il se lancera a chaque fois et pout ca on va créer un fichier "mattermost.service" :
```
[thomas@mattermost ~]$ sudo cat /etc/systemd/system/mattermost.service
[Unit]
Description=Mattermost
After=syslog.target network.target mysqld.service

[Service]
Type=notify
WorkingDirectory=/opt/mattermost
User=mattermost
ExecStart=/opt/mattermost/bin/mattermost
PIDFile=/var/spool/mattermost/pid/master.pid
TimeoutStartSec=3600
KillMode=mixed
LimitNOFILE=49152

[Install]
WantedBy=multi-user.target
```

```
[thomas@mattermost ~]$ sudo systemctl daemon-reload

[thomas@mattermost ~]$ sudo systemctl status mattermost.service
● mattermost.service - Mattermost
   Loaded: loaded (/etc/systemd/system/mattermost.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2022-12-07 21:35:04 CET; 2min 15s ago
```

### Maîtrise de la solution

Une fois en place, posez-vous les questions pour comprendre ce qui a été mis en place, plus en détail. Réfléchissez avec les outils et concepts qu'on a vus en cours :

- y a-t-il un fichier de conf ?  
*Il y a un fichier de conf pour mattermost qui s'appelle config.json*  
- combien de programmes y a-t-il ?  
*Il y a une programme mattermost et un programme mysql qui va avec.*  
  - et donc quels processus ?  
*Il y a 5 processus qui tourne pour mattermost sur la machine dédiée :*
```
[thomas@mattermost ~]$ sudo ps -ef |grep mattermost
matterm+    1575       1 15 10:49 ?        00:00:09 /opt/mattermost/bin/mattermost
matterm+    1588    1575  0 10:49 ?        00:00:00 plugins/com.mattermost.plugin-channel-export/server/dist/plugin-linux-amd64
matterm+    1592    1575  0 10:49 ?        00:00:00 plugins/com.mattermost.nps/server/dist/plugin-linux-amd64
matterm+    1596    1575  0 10:49 ?        00:00:00 plugins/com.mattermost.apps/server/dist/plugin-linux-amd64
matterm+    1605    1575  0 10:49 ?        00:00:00 plugins/com.mattermost.calls/server/dist/plugin-linux-amd64
```
*Et il y a 1 processus qui tourne pour mysql sur la machine dédiée :*
```
[thomas@database ~]$ sudo ps -ef | grep mysql
mysql       1659       1  1 10:49 ?        00:00:03 /usr/sbin/mysqld
```
- sous quelle identité, quel user, tournent chacun de ces processus ?  
*Les processus mattermost tourne sous l'utilisateur mattermost et le processus mysqld tourne sous l'utilisateur mysql.*  
- où sont stockées les données de l'application ?  
*Les données de l'application sont stockées dans une base de données mysqld sur une machine à part.*  
- sur quel(s) port(s) écoute la solution ?  
*La solution mattermost écoute sur le port 8065, qu'il faut d'ailleurs préciser pour se connecter sur notre navigateur à notre solution.*  
- l'architecture et/ou la conf de la solution respectent-elles les bonnes pratiques élémentaires ?  
  - un service par machine (ou un service par conteneur)  
*Oui il y a bien un seul service par machine, une pour mattermost et une pour mysqld.*  
  - gestion correcte des utilisateurs et des permissions  
*Sur la base de données il y a une gestion des permissions avec un utilisateur "mmuser" qui à les droits sur la database "mattermost", et sur l'application il y a un user crée "administrateur" qui à l'accès au pannel administrateur.*



## III. Fail2ban

| nom des VM | IP des VM  |
| ---------- | ---------- |
| mattermost | 10.104.1.10|
| ---------- | ---------- |
| database   | 10.104.1.20|
| ---------- | ---------- |


### 1. Installation de epel-release et fail2ban sur mattermost et data.


```
[thomas@mattermost ~]$ sudo dnf install -y epel-release
Installed:
  epel-release-8-18.el8.noarch

Complete!
```

```
[thomas@mattermost ~]$ sudo dnf install fail2ban fail2ban-firewalld
Installed:
  esmtp-1.2-15.el8.x86_64
  fail2ban-0.11.2-1.el8.noarch
  fail2ban-firewalld-0.11.2-1.el8.noarch
  fail2ban-sendmail-0.11.2-1.el8.noarch
  fail2ban-server-0.11.2-1.el8.noarch
  libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-2.el8.x86_64
  python3-pip-9.0.3-22.el8.rocky.0.noarch
  python36-3.6.8-38.module+el8.5.0+671+195e4563.x86_64

Complete!
```

```
[thomas@mattermost ~]$ sudo systemctl start fail2ban
[thomas@mattermost ~]$ sudo systemctl enable fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[thomas@mattermost ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service;
   Active: active (running) since Sat 2022-12-10 15:11:36 CET;
```

### 2. Configuration de fail2ban.

```
[thomas@mattermost ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

[thomas@mattermost ~]$ sudo cat /etc/fail2ban/jail.local

# "bantime" is the number of seconds that a host is banned.
bantime  = 1h

# A host is banned if it has generated "maxretry" during the l$# seconds.
findtime  = 1h

# "maxretry" is the number of failures before a host get banne$maxretry = 3
```

```
[thomas@mattermost ~]$ sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local

[thomas@mattermost ~]$ sudo systemctl restart fail2ban
[thomas@mattermost ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; 
   Active: active (running) since Sat 2022-12-10 15:14:18 CET;
     Docs: man:fail2ban(1)
  Process: 2092 ExecStop=/usr/bin/fail2ban-client stop (code)
  Process: 2093 ExecStartPre=/bin/mkdir -p /run/fail2ban (code)
 Main PID: 2095 (fail2ban-server)
```

### 3. Securing SSH service with Fail2ban

```
[thomas@mattermost ~]$ sudo cat /etc/fail2ban/jail.d/sshd.local

[sshd]
enabled = true

# Override the default global configuration
# for specific jail sshd
bantime = 1d
maxretry = 3
```

```
[thomas@mattermost ~]$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd

[thomas@mattermost ~]$ sudo fail2ban-client get sshd maxretry
3
```

**Bien sur on install fail2ban sur toutes nos machines pour plus de sécurité et pour empêcher les attaques par brutes forces sur nos connexions SSH**



## IV. netdata

| nom des VM | IP des VM  |
| ---------- | ---------- |
| mattermost | 10.104.1.10|
| ---------- | ---------- |
| database   | 10.104.1.20|
| ---------- | ---------- |

### 1. Installation de Netdata sur mattermost et data.

```
[thomas@mattermost ~]$ sudo dnf install -y wget
Last metadata expiration check: 0:12:21 ago on Sat 10 Dec 2022 03:10:32 PM CET.
Package wget-1.19.5-10.el8.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

```
[thomas@mattermost ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-telemetry
Complete!
OK
Successfully installed the Netdata Agent.
```

```
[thomas@mattermost ~]$ sudo systemctl start netdata
[thomas@mattermost ~]$ sudo systemctl enable netdata
[thomas@mattermost ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Sat 2022-12-10 15:26:56 CET; 1s ago
  Process: 3516 ExecStartPre=/bin/chown -R netdata /run/netdata (code=exited, status=0/SUCCESS)
  Process: 3513 ExecStartPre=/bin/mkdir -p /run/netdata (code=exited, status=0/SUCCESS)
  Process: 3511 ExecStartPre=/bin/chown -R netdata /var/cache/netdata (code=exited, status=0/SUCCESS)
  Process: 3510 ExecStartPre=/bin/mkdir -p /var/cache/netdata (code=exited, status=0/SUCCESS)
 Main PID: 3518 (netdata)
    Tasks: 43 (limit: 4908)
```

### 2. Le Firewall.

```
[thomas@mattermost ~]$ sudo firewall-cmd --add-port=19999/tcp -
-permanent
success
[thomas@mattermost ~]$ sudo firewall-cmd --reload
success
[thomas@mattermost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 8065/tcp 19999/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

![netdata](./images/netdata-tp5.PNG)


### 3. Mise en place Webhook pour Netdata

**On va sur notre serveur discord et on va venir créer un Webhook, puis on va prendre l'URL de ce Webhook qu'on va venir mettre dans notre configuration de Netdata :**

```
[thomas@mattermost netdata]$ sudo cat health_alarm_notify.conf | grep DISCORD
SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1051147306285678612/AerhZXrWp6J0luWbNtu71e_7vVHNZC4Jq6AUOuEr8j9Dt9eMeh9zISNpZFBzJJdG3hME"
DEFAULT_RECIPIENT_DISCORD="général"
```

**Ensuite on va venir tester si ca marche en faisant un "stress test" sur notre machine :**

```
[thomas@mattermost ~]$ sudo dnf install stress
Installed:
  stress-1.0.4-24.el8.x86_64

Complete!

[thomas@mattermost ~]$ stress --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 600s
stress: info: [4679] dispatching hogs: 8 cpu, 4 io, 2 vm, 0 hdd
```

**Et on peut voir qu'on a des alertes discord sur notre serveur par Netdata :**

![alertes](./images/discord_alertes.PNG)


## V. Proxy et Certificat HTTPS

### 1 : Reverse Proxy

Un reverse proxy est donc une machine que l'on place devant un autre service afin d'accueillir les clients et servir d'intermédiaire entre le client et le service.

# I. Setup

🖥️ **VM `proxy.tp5.linux` avec une adresse IP : `10.104.1.30`**

**Bien sur on fait la configuration de la machine virtuelle comme on a fait pour les autres machines**
**On met en place aussi Fail2ban et netdata, qu'on a mis en place sur les autres machines, pour plus de sécurité**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`
```
[thomas@proxy ~]$ sudo dnf install nginx -y
Installed:
  nginx-1:1.20.1-10.el9.x86_64     nginx-filesystem-1:1.20.1-10.el9.noarch     rocky-logos-httpd-90.11-1.el9.noarch

Complete!
```

- démarrer le service `nginx`
```
[thomas@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[thomas@proxy ~]$ sudo systemctl start nginx
[thomas@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enab>
   Active: active (running) since Sun 2022-12-11 17:40:00 CET;>
  Process: 25469 ExecStart=/usr/sbin/nginx (code=exited, statu>
  Process: 25467 ExecStartPre=/usr/sbin/nginx -t (code=exited,>
  Process: 25466 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (c>
 Main PID: 25471 (nginx)
    Tasks: 2 (limit: 4935)
```

- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX
```
[thomas@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[thomas@proxy ~]$ sudo firewall-cmd --reload
success
[thomas@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 80/tcp
```

➜ **Configurer NGINX**

```
[thomas@proxy nginx]$ sudo cat conf.d/proxy.conf

server {
# On indique le nom que client va saisir pour accéder au service
    server_name mattermost.tp5.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.104.1.10:8065;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```

➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `mattermost.tp5.linux`
- c'est à dire que `mattermost.tp5.linux` doit pointer vers l'IP de `proxy.tp5.linux`
```
# Copyright (c) 1993-2009 Microsoft Corp.
#
# This is a sample HOSTS file used by Microsoft TCP/IP for Windows.
#
# This file contains the mappings of IP addresses to host names. Each
# entry should be kept on an individual line. The IP address should
# be placed in the first column followed by the corresponding host name.
# The IP address and the host name should be separated by at least one
# space.
#
# Additionally, comments (such as these) may be inserted on individual
# lines or following the machine name denoted by a '#' symbol.
#
# For example:
#
#      102.54.94.97     rhino.acme.com          # source server
#       38.25.63.10     x.acme.com              # x client host

# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.104.1.30	mattermost.tp5.linux
```

# II. HTTPS

**Pour avoir une connexion HTTPS, il faut plusieurs étapes :**

**- on génère une paire de clés sur le serveur `proxy.tp5.linux` :**
```
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
```

**On met la clé et le certificat dans un fichier spécial :**
```
sudo mv server.crt /etc/pki/tls/certs/;
sudo mv server.key /etc/pki/tls/private/;
```

- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP
```
listen 443 ssl;
    ssl_certificate     /etc/pki/tls/certs/server.crt;
    ssl_certificate_key /etc/pki/tls/private/server.key;
```

**On pense aussi à ouvrir notre port sur le firewall pour laisser passer la connexion https :**

```
[thomas@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[thomas@proxy ~]$ sudo firewall-cmd --reload
success
[thomas@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 80/tcp 443/tcp
```

**Et au final on arrive sur notre site en tapant : "https://mattermost.tp5.linux"**

![https-mattermost](./images/HTTPS_mattermost.PNG)
