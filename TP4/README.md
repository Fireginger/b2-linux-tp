# TP4 : Conteneurs

Dans ce TP on va aborder plusieurs points autour de la conteneurisation : 

- Docker et son empreinte sur le système
- Manipulation d'images
- `docker-compose`

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.104.1.0/24`.**

➜ Chaque **création de machines** sera indiquée par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/engine/install/)
```
[thomas@docker1 ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo

[thomas@docker1 ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io
[...]
Installed:
  checkpolicy-3.3-1.el9.x86_64                               container-selinux-3:2.188.0-1.el9_0.noarch
  containerd.io-1.6.10-3.1.el9.x86_64                        docker-ce-3:20.10.21-3.el9.x86_64
  docker-ce-cli-1:20.10.21-3.el9.x86_64                      docker-ce-rootless-extras-20.10.21-3.el9.x86_64
  docker-scan-plugin-0.21.0-3.el9.x86_64                     fuse-common-3.10.2-5.el9.0.1.x86_64
  fuse-overlayfs-1.9-1.el9_0.x86_64                          fuse3-3.10.2-5.el9.0.1.x86_64
  fuse3-libs-3.10.2-5.el9.0.1.x86_64                         libslirp-4.4.0-7.el9.x86_64
  policycoreutils-python-utils-3.3-6.el9_0.noarch            python3-audit-3.0.7-101.el9_0.2.x86_64
  python3-libsemanage-3.3-2.el9.x86_64                       python3-policycoreutils-3.3-6.el9_0.noarch
  python3-setools-4.4.0-4.el9.x86_64                         python3-setuptools-53.0.0-10.el9.noarch
  slirp4netns-1.2.0-2.el9_0.x86_64                           tar-2:1.34-3.el9.x86_64

Complete!
```
- démarrer le service `docker` avec une commande `systemctl`
```
[thomas@docker1 ~]$ sudo systemctl start docker
[thomas@docker1 ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-24 16:49:33 CET; 4s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 2946 (dockerd)
      Tasks: 7
     Memory: 23.6M
        CPU: 78ms
     CGroup: /system.slice/docker.service
             └─2946 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
```
- ajouter votre utilisateur au groupe `docker`
```
[thomas@docker1 ~]$ sudo usermod -aG docker $(whoami)
```
- déconnectez-vous puis relancez une session pour que le changement prenne effet


➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**

```
# Info sur l'install actuelle de Docker
[thomas@docker1 ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
  scan: Docker Scan (Docker Inc., v0.21.0)

# Liste des conteneurs actifs
[thomas@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

# Liste de tous les conteneurs
[thomas@docker1 ~]$ docker ps -a
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES

# Liste des images disponibles localement
[thomas@docker1 ~]$ docker images
REPOSITORY   TAG       IMAGE ID   CREATED   SIZE

# Lancer un conteneur debian
[thomas@docker1 ~]$ docker run debian
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
a8ca11554fce: Pull complete
Digest: sha256:3066ef83131c678999ce82e8473e8d017345a30f5573ad3e44f62e5c9c46442b
Status: Downloaded newer image for debian:latest

# -d sert à mettre un conteneur en tâche de fond (-d pour daemon)
[thomas@docker1 ~]$ docker run -d debian sleep 99999
b490fe90196ab1e07cd6d917f364a2d0634779f28a73881b08dc16bce58a953f

# à l'inverse, -it sert à avoir un shell interactif (incompatible avec -d)
[thomas@docker1 ~]$ docker run -it debian bash
root@6a9b42b4e38a:/#

# Consulter les logs d'un conteneur
$ docker ps # on repère l'ID/le nom du conteneur voulu
[thomas@docker1 ~]$ docker logs b490fe90196a

# supprimer un conteneur donné, même s'il est allumé
[thomas@docker1 ~]$ docker rm -f b490fe90196a
b490fe90196a
[thomas@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

## 3. Lancement de conteneurs

La commande pour lancer des conteneurs est `docker run`.

🌞 **Utiliser la commande `docker run`**

- lancer un conteneur `nginx`
  - l'app NGINX doit avoir un fichier de conf personnalisé
  - l'app NGINX doit servir un fichier `index.html` personnalisé
  - l'application doit être joignable grâce à un partage de ports
  - vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
  - le conteneur devra avoir un nom


```
[thomas@docker1 ~]$ docker run -d --memory="512m" --cpus="1.0" --name web -v $(pwd)/index.html:/index.html -v $(pwd)/ngi
nx.conf:/nginx.conf nginx
Unable to find image 'nginx:latest' locally
latest: Pulling from library/nginx
a603fa5e3b41: Pull complete
c39e1cda007e: Pull complete
90cfefba34d7: Pull complete
a38226fb7aba: Pull complete
62583498bae6: Pull complete
9802a2cfdb8d: Pull complete
Digest: sha256:e209ac2f37c70c1e0e9873a5f7231e91dcd83fdf1178d8ed36c2ec09974210ba
Status: Downloaded newer image for nginx:latest
74cf1e8ae8dcc45ecfcee88ed704aa7c16367ddd6955196c9f0fcb1e1067b69a
[thomas@docker1 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS     NAMES
74cf1e8ae8dc   nginx     "/docker-entrypoint.…"   7 seconds ago   Up 5 seconds   80/tcp    web
```

# II. Images

La construction d'image avec Docker est basée sur l'utilisation de fichiers `Dockerfile`.

L'idée est la suivante :

- vous créez un dossier de travail
- vous vous déplacez dans ce dossier de travail
- vous créez un fichier `Dockerfile`
```
[thomas@docker1 images]$ cat Dockerfile
FROM debian

RUN apt update -y

RUN apt install -y apache2

COPY apache2.conf /etc/apache2/apache2.conf

COPY index.html /usr/share/nginx/html/index.html

ADD . /etc/apache2/logs/

CMD [ "apache2", "-D", "FOREGROUND" ]

```
- vous exécutez une commande `docker build . -t <IMAGE_NAME>`
- une image est produite, visible avec la commande `docker images`
```
[thomas@docker1 images]$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED             SIZE
my_apache    latest    685239830635   20 minutes ago      253MB
```

## 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**

- image de base (celle que vous voulez : debian, alpine, ubuntu, etc.)
  - une image du Docker Hub
  - qui ne porte aucune application par défaut
- vous ajouterez
  - mise à jour du système
  - installation de Apache (pour les systèmes debian, le serveur Web apache s'appelle `apache2` et non pas `httpd` comme sur Rocky)
  - page d'accueil Apache HTML personnalisée

```
[thomas@docker1 images]$ docker build . -t my_apache
Sending build context to Docker daemon  4.608kB
Step 1/7 : FROM debian
 ---> c31f65dd4cc9
Step 2/7 : RUN apt update -y
 ---> Using cache
 ---> 37f480ca8411
Step 3/7 : RUN apt install -y apache2
 ---> Using cache
 ---> 54cf0956e9b1
Step 4/7 : COPY apache2.conf /etc/apache2/apache2.conf
 ---> b87ca3f30ab8
Step 5/7 : COPY index.html /usr/share/nginx/html/index.html
 ---> 9bbfe34f9c3d
Step 6/7 : ADD . /etc/apache2/logs/
 ---> a75bf43f8d0e
Step 7/7 : CMD [ "apache2", "-D", "FOREGROUND" ]
 ---> Running in cc7f285f45e6
Removing intermediate container cc7f285f45e6
 ---> 685239830635
Successfully built 685239830635
Successfully tagged my_apache:latest
```

```
[thomas@docker1 images]$ docker images
REPOSITORY   TAG       IMAGE ID       CREATED             SIZE
my_apache    latest    685239830635   20 minutes ago      253MB
```

```
[thomas@docker1 images]$ docker run -p 8888:80 my_apache
```

```
[thomas@docker1 images]$ curl localhost:8888

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Apache2 Debian Default Page: It works</title>
    <style type="text/css" media="screen">
  * {
    margin: 0px 0px 0px 0px;
    padding: 0px 0px 0px 0px;
  }
```

📁 **`Dockerfile`**


![Dockerfile](./Dockerfile)


# III. `docker-compose`

## 1. Intro

➜ **Installer `docker-compose` sur la machine**

- en suivant [la doc officielle](https://docs.docker.com/compose/install/)

```
[thomas@docker1 ~]$ sudo dnf install docker-compose-plugin
Installed:
  docker-compose-plugin-2.12.2-3.el9.x86_64

Complete!
```

```
[thomas@docker1 ~]$ docker compose version
Docker Compose version v2.12.2
```

`docker-compose` est un outil qui permet de lancer plusieurs conteneurs en une seule commande.

Le principe de fonctionnement de `docker-compose` :

- on écrit un fichier qui décrit les conteneurs voulus
  - c'est le `docker-compose.yml`
  - tout ce que vous écriviez sur la ligne `docker run` peut être écrit sous la forme d'un `docker-compose.yml`
- on se déplace dans le dossier qui contient le `docker-compose.yml`


## 2. Make your own meow

Pour cette partie, vous utiliserez une application à vous que vous avez sous la main.

N'importe quelle app fera le taff, un truc dév en cours, en temps perso, au taff, peu importe.

🌞 **Conteneurisez votre application**

```
[thomas@docker1 images]$ sudo dnf install git -y
[...]
Complete!
```

On clone notre projet :
```
[thomas@docker1 ~]$ git clone https://github.com/Fireginger/hangman-web
Cloning into 'hangman-web'...
remote: Enumerating objects: 30, done.
remote: Counting objects: 100% (30/30), done.
remote: Compressing objects: 100% (28/28), done.
remote: Total 30 (delta 0), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (30/30), 9.81 KiB | 1.40 MiB/s, done.


[thomas@docker1 ~]$ sudo chown -R root:root hangman-web/
```

On créé l'image :
```
[thomas@docker1 hangman-web]$ docker build . -t hangman-web-go
Successfully built ff7df8e35b67
Successfully tagged hangman-web-go:latest

[thomas@docker1 hangman-web]$ docker images
REPOSITORY       TAG       IMAGE ID       CREATED          SIZE
hangman-web-go   latest    ff7df8e35b67   10 seconds ago   847MB
```

On ouvre le port nécessaire pour que ca marche :
```
[thomas@docker1 hangman-web]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[thomas@docker1 hangman-web]$ sudo firewall-cmd --reload
success
```
Et pour lancer le docker compose c'est :
```
[thomas@docker1 hangman-web]$ docker compose up -d
 ⠴ Container hangman-web-hangman-1  Starting
```
Et c'est bon c'est terminé.
