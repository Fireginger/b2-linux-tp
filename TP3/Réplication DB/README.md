# Module 2 : Réplication de base de données

Il y a plein de façons de mettre en place de la réplication de base données de type MySQL (comme MariaDB).

Une réplication simple est une configuration de type "master-slave". Un des deux serveurs est le *master* l'autre est un *slave*.

Le *master* est celui qui reçoit les requêtes SQL (des applications comme NextCloud) et qui les traite.
Le *slave* ne fait que répliquer les donneés que le *master* possède.

Pour ce module, vous aurez besoin d'un deuxième serveur de base de données.

✨ **Bonus** : Faire en sorte que l'utilisateur créé en base de données ne soit utilisable que depuis l'autre serveur de base de données

- inspirez-vous de la création d'utilisateur avec `CREATE USER` effectuée dans le TP2

✨ **Bonus** : Mettre en place un setup *master-master* où les deux serveurs sont répliqués en temps réel, mais les deux sont capables de traiter les requêtes.

**Installation :**

```
[thomas@dbslave ~]$ sudo dnf install mariadb-server -y
Last metadata expiration check: 0:03:10 ago on Fri 18 Nov 2022 02:59:19 PM CET.
Package mariadb-server-3:10.5.16-2.el9_0.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

**Démarrage du service mariadb :**
```
[thomas@dbslave ~]$ sudo systemctl start mariadb
[thomas@dbslave ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

```
[thomas@dbslave ~]$ sudo mysql_secure_installation

NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none):
OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n
 ... skipping.

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] y
New password:
Re-enter new password:
Password updated successfully!
Reloading privilege tables..
 ... Success!


By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] y
 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] y
 ... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] y
 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

**Sur le Master (la permière DB) :**
```
[thomas@db ~]# sudo nano /etc/my.cnf.d/mariadb-server.cnf

[mysqld]
# add follows in [mysqld] section : get binary logs
log-bin=mysql-bin
# define server ID (uniq one)
server-id=101
```

```
[thomas@db ~]# sudo systemctl restart mariadb

[thomas@db ~]# mysql -u root -p
```
```sql
# create user (set any password for 'password' section)

MariaDB [(none)]> grant replication slave on *.* to repl_user@'%' identified by 'thomas'; 
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> flush privileges; 
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> exit
Bye
```

```
[thomas@dbslave ~]# sudo nano /etc/my.cnf.d/mariadb-server.cnf

[mysqld]
# add follows in [mysqld] section : get binary logs
log-bin=mysql-bin
# define server ID (uniq one)
server-id=102
# read only yes
read_only=1
# define own hostname
report-host=dbslave.tp3.linux
```

```
[thomas@dbslave ~]# systemctl restart mariadb

# create a directory and get Backup Data
[thomas@db ~]# mkdir /home/mariadb_backup
[thomas@db ~]# mariabackup --backup --target-dir /home/mariadb_backup -u root -p thomas
[00] 2021-08-03 16:14:27 Connecting to MySQL server host: localhost, user: root, password: set, port: not set, socket: not set
[00] 2021-08-03 16:14:27 Using server version 10.3.28-MariaDB
mariabackup based on MariaDB server 10.3.28-MariaDB Linux (x86_64)
[00] 2021-08-03 16:14:27 uses posix_fadvise().
[00] 2021-08-03 16:14:27 cd to /var/lib/mysql/
.....
.....
[00] 2021-08-03 16:14:30         ...done
[00] 2021-08-03 16:14:30 Redo log (from LSN 1640838 to 1640847) was copied.
[00] 2021-08-03 16:14:30 completed OK!
```

**stop MariaDB and remove existing data**

```
[thomas@dbslave ~]# systemctl stop mariadb

[thomas@dbslave ~]# rm -rf /var/lib/mysql/*
```

**transfered backup data**

```
[thomas@db ~]$ sudo zip -r mariadb_backup.zip /home/mariadb_backup/

[thomas@dbslave ~]$ sftp thomas@10.102.1.12
Connected to 10.102.1.12.
sftp> get mariadb_backup.zip
quit
[thomas@dbslave ~]$ sudo unzip mariadb_backup.zip
```

**run prepare task before restore task (OK if [completed OK])**

```
[thomas@dbslave ~]# mariabackup --prepare --target-dir mariadb_backup
mariabackup based on MariaDB server 10.3.28-MariaDB Linux (x86_64)
[00] 2021-08-03 16:23:30 cd to /home/thomas/mariadb_backup/
[00] 2021-08-03 16:23:30 open files limit requested 0, set to 1024
.....
.....
2021-08-03 16:23:30 0 [Note] InnoDB: Starting crash recovery from checkpoint LSN=1640838
[00] 2021-08-03 16:23:30 Last binlog file , position 0
[00] 2021-08-03 16:23:30 completed OK!
```

**run restore**
```
[thomas@dbslave ~]# mariabackup --copy-back --target-dir mariadb_backup
mariabackup based on MariaDB server 10.3.28-MariaDB Linux (x86_64)
[01] 2021-08-03 16:23:46 Copying ibdata1 to /var/lib/mysql/ibdata1
[01] 2021-08-03 16:23:46         ...done
.....
.....
[01] 2021-08-03 16:23:46 Copying ./xtrabackup_info to /var/lib/mysql/xtrabackup_info
[01] 2021-08-03 16:23:46         ...done
[00] 2021-08-03 16:23:46 completed OK!
```

```
[thomas@dbslave ~]# chown -R mysql. /var/lib/mysql

[thomas@dbslave ~]# systemctl start mariadb
```

**confirm [File] and [Position] value of master log**

```
[thomas@dbslave ~]# cat /root/mariadb_backup/xtrabackup_binlog_info
mysql-bin.000003        385     0-101-2
```

**set replication**

```
[thomas@dbslave ~]# mysql -u root -p
```

```sql
# master_host=(Master Host IP address)
# master_user=(replication user)
# master_password=(replication user password)
# master_log_file=([File] value confirmed above)
# master_log_pos=([Position] value confirmed above)

MariaDB [(none)]> change master to 
master_host='10.102.1.12',
master_user='repl_user',
master_password='thomas',
master_log_file='mysql-bin.000003',
master_log_pos=385;
Query OK, 0 rows affected (0.191 sec)
```

**start replication**

```sql
MariaDB [(none)]> start slave; 
Query OK, 0 rows affected (0.00 sec)
```

**show status**

```sql
MariaDB [(none)]> show slave status\G
*************************** 1. row ***************************
                Slave_IO_State: Waiting for master to send event
                   Master_Host: 10.102.1.12
                   Master_User: repl_user
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mysql-bin.000003
           Read_Master_Log_Pos: 385
                Relay_Log_File: mariadb-relay-bin.000002
                 Relay_Log_Pos: 555
         Relay_Master_Log_File: mysql-bin.000003
              Slave_IO_Running: Yes
             Slave_SQL_Running: Yes
               Replicate_Do_DB:
           Replicate_Ignore_DB:
            Replicate_Do_Table:
        Replicate_Ignore_Table:
       Replicate_Wild_Do_Table:
   Replicate_Wild_Ignore_Table:
                    Last_Errno: 0
                    Last_Error:
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 385
               Relay_Log_Space: 866
               Until_Condition: None
                Until_Log_File:
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File:
            Master_SSL_CA_Path:
               Master_SSL_Cert:
             Master_SSL_Cipher:
                Master_SSL_Key:
         Seconds_Behind_Master: 0
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error:
                Last_SQL_Errno: 0
                Last_SQL_Error:
   Replicate_Ignore_Server_Ids:
              Master_Server_Id: 101
                Master_SSL_Crl:
            Master_SSL_Crlpath:
                    Using_Gtid: No
                   Gtid_IO_Pos:
       Replicate_Do_Domain_Ids:
   Replicate_Ignore_Domain_Ids:
                 Parallel_Mode: optimistic
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
              Slave_DDL_Groups: 0
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 0
1 row in set (0.000 sec)
```
