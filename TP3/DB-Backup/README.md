# Module 3 : Sauvegarde de base de données

Dans cette partie le but va être d'écrire un script `bash` qui récupère le contenu de la base de données utilisée par NextCloud, afin d'être en mesure de restaurer les données plus tard si besoin.

Le script utilisera la commande `mysqldump` qui permet de récupérer le contenu de la base de données sous la forme d'un fichier `.sql`.

## I. Script dump

➜ **Créer un utilisateur DANS LA BASE DE DONNEES**

- l'utilisateur doit pouvoir se connecter depuis `localhost`
- il doit avoir les droits sur la base de données `nextcloud` qu'on a créé au TP2
```sql
MariaDB [(none)]> CREATE USER 'restore'@'10.102.1.11' IDENTIFIED BY 'toto';
Query OK, 0 rows affected (0.008 sec)

MariaDB [(none)]> RENAME USER 'restore'@'10.102.1.11' to 'restore'@'localhost';
Query OK, 0 rows affected (0.010 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'restore'@'localhost';
Query OK, 0 rows affected (0.003 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

```
[thomas@db ~]$ sudo dnf install tar -y
Last metadata expiration check: 0:15:21 ago on Wed 23 Nov 2022 11:01:59 AM CET.
Package tar-2:1.34-3.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

➜ **Ecrire le script `bash`**

- il s'appellera `tp3_db_dump.sh`
- il devra être stocké dans le dossier `/srv` sur la machine `db.tp2.linux`
- le script doit commencer par un *shebang* qui indique le chemin du programme qui exécutera le contenu du script

- le script doit contenir une commande `mysqldump`
  - qui récupère le contenu de la base de données `nextcloud`
  - en utilisant l'utilisateur précédemment créé
- le fichier `.sql` produit doit avoir **un nom précis** :
  - il doit comporter le nom de la base de données dumpée
  - il doit comporter la date, l'heure la minute et la seconde où a été effectué le dump
  - par exemple : `db_nextcloud_2211162108.sql`
- enfin, le fichier `sql` doit être compressé
  - au format `.zip` ou `.tar.gz`
  - le fichier produit sera stocké dans le dossier `/srv/db_dumps/`
  - il doit comporter la date, l'heure la minute et la seconde où a été effectué le dump

```
[thomas@db srv]$ sudo cat tp3_db_dump.sh
#!/bin/bash
# Last Update : 23/11/2022
# This script will dump the database and save it to a file

# Set the variables
user='restore'
passwd='toto'
db='nextcloud'
ip_serv='localhost'
datesauv=$(date '+%y%m%d%H%M%S')
name="${db}_${datesauv}"
outputpath="/srv/db_dumps/${name}.sql"

# Dump

echo "Backup started for database - ${db}."
mysqldump -h ${ip_serv} -u ${user} -p${passwd} --skip-lock-tables --databases ${db} > $outputpath
if [[ $? == 0 ]]
then
        gzip -c $outputpath > "${outputpath}.gz"
        rm -f $outputpath
        echo "Backup successfully completed."
else
        echo "Backup failed."
        rm -f outputpath
        exit 1
fi
```

**Bien sur on change les permissions du fichier pour qu'il puisse s'exécuter :**
```
[thomas@db srv]$ sudo chmod 744 tp3_db_dump.sh
[thomas@db srv]$ ls -al
total 4
drwxr-xr-x.  3 root root  44 Nov 23 11:23 .
dr-xr-xr-x. 18 root root 235 Sep 30 15:28 ..
drwxr-xr-x.  2 root root   6 Nov 23 11:20 db_dumps
-rwxr--r--.  1 root root 654 Nov 23 11:23 tp3_db_dump.sh
```

## II. Clean it

On va rendre le script un peu plus propre vous voulez bien ?

➜ **Utiliser des variables** déclarées en début de script pour stocker les valeurs suivantes :

- utilisateur de la base de données utiliser pour dump
- son password
- le nom de la base
- l'IP à laquelle la commande `mysqldump` se connecte
- le nom du fichier `.tar.gz` ou `.zip` produit par le script

**On a déja fait cette partie dans la partie d'avant dans notre premier script**

---

➜ **Commentez le script**

- au minimum un en-tête sous le shebang
  - date d'écriture du script
  - nom/pseudo de celui qui l'a écrit
  - un résumé TRES BREF de ce que fait le script

**Ajout du créateur du script :**
```
[thomas@db srv]$ sudo cat tp3_db_dump.sh
#!/bin/bash
# Last Update : 23/11/2022
# This script will dump the database and save it to a file
# Creator : Thomas Eveillard
```

---

➜ **Environnement d'exécution du script**

- créez un utilisateur sur la machine `db.tp2.linux`
  - il s'appellera `db_dumps`
  - son homedir sera `/srv/db_dumps/`
  - son shell sera `/usr/bin/nologin`

```
[thomas@db ~]$ sudo useradd db_dumps -m -d /srv/db_dumps/ -s /usr/bin/nologin
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
useradd: warning: the home directory /srv/db_dumps/ already exists.
useradd: Not copying any file from skel directory into it.
```

- cet utilisateur sera celui qui lancera le script
- le dossier `/srv/db_dumps/` doit appartenir au user `db_dumps`
```
[thomas@db srv]$ sudo chown db_dumps:db_dumps tp3_db_dump.sh
[thomas@db srv]$ ls -al
[...]
-rwxr--r--.  1 db_dumps db_dumps 683 Nov 23 11:27 tp3_db_dump.sh
```

- pour tester l'exécution du script en tant que l'utilisateur `db_dumps`, utilisez la commande suivante :

```
[thomas@db db_dumps]$ sudo -u db_dumps /srv/tp3_db_dump.sh
Backup started for database - nextcloud.
Backup successfully completed.

[thomas@db db_dumps]$ ls -al
total 52
drwxr-xr-x. 2 db_dumps db_dumps    43 Nov 23 11:49 .
drwxr-xr-x. 3 root     root        44 Nov 23 11:46 ..
-rw-r--r--. 1 db_dumps db_dumps 52285 Nov 23 11:49 nextcloud_221123114925.sql.gz
```

**On essaye de Unzip la backup qu'on vient de faire pour voir si ca marche bien :**
```
[thomas@db db_dumps]$ sudo gzip -d nextcloud_221123114925.sql.gz
[thomas@db db_dumps]$ cat nextcloud_221123114925.sql
-- MariaDB dump 10.19  Distrib 10.5.16-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: nextcloud
-- ------------------------------------------------------
-- Server version       10.5.16-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
```


## III. Service et timer

➜ **Créez un *service*** système qui lance le script

- inspirez-vous du *service* créé à la fin du TP1
- la seule différence est que vous devez rajouter `Type=oneshot` dans la section `[Service]` pour indiquer au système que ce service ne tournera pas à l'infini (comme le fait un serveur web par exemple) mais se terminera au bout d'un moment
- vous appelerez le service `db-dump.service`
- assurez-vous qu'il fonctionne en utilisant des commandes `systemctl`

```
[thomas@db db_dumps]$ cd /etc/systemd/system
[thomas@db system]$ cat db_dump.service
[Unit]
Description=Dump the nextcloud database

[Service]
ExecStart=/srv/tp3_db_dump.sh
Type=oneshot
User=db_dumps
WorkingDirectory=/srv/db_dumps

[Install]
WantedBy=multi-user.target
```

**Ajout des permissions pour le script**
```
[thomas@db srv]$ sudo chmod 754 tp3_db_dump.sh
[thomas@db srv]$ ls -al
total 4
drwxr-xr-x.  3 root     root      44 Nov 23 11:46 .
dr-xr-xr-x. 18 root     root     235 Sep 30 15:28 ..
drwxr-xr-x.  2 db_dumps db_dumps  40 Nov 23 11:50 db_dumps
-rwxr-xr--.  1 db_dumps db_dumps 683 Nov 23 11:48 tp3_db_dump.sh
```

**On lance le service :**
```
[thomas@db ~]$ sudo systemctl start db_dump
[thomas@db ~]$ sudo systemctl status db_dump
○ db_dump.service - Dump the nextcloud database
     Loaded: loaded (/etc/systemd/system/db_dump.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

Nov 23 11:56:07 db.tp2.linux systemd[1]: Starting Dump the nextcloud database...
Nov 23 11:56:07 db.tp2.linux tp3_db_dump.sh[1307]: Backup started for database - nextcloud.
Nov 23 11:56:07 db.tp2.linux tp3_db_dump.sh[1307]: Backup successfully completed.
Nov 23 11:56:07 db.tp2.linux systemd[1]: db_dump.service: Deactivated successfully.
Nov 23 11:56:07 db.tp2.linux systemd[1]: Finished Dump the nextcloud database.
```

➜ **Créez un *timer*** système qui lance le *service* à intervalles réguliers

- le fichier doit être créé dans le même dossier
- le fichier doit porter le même nom
- l'extension doit être `.timer` au lieu de `.service`
- ainsi votre fichier s'appellera `db-dump.timer`

```
[thomas@db system]$ sudo cat db_dump.timer
[Unit]
Description=Run service X

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
```

- une fois le fichier créé :

```
[thomas@db ~]$ sudo systemctl daemon-reload

[thomas@db ~]$ sudo systemctl start db_dump.timer

[thomas@db ~]$ sudo systemctl enable db_dump.timer
Created symlink /etc/systemd/system/timers.target.wants/db_dump.timer → /etc/systemd/system/db_dump.timer.

[thomas@db ~]$ sudo systemctl status db_dump.timer
● db_dump.timer - Run service X
     Loaded: loaded (/etc/systemd/system/db_dump.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Wed 2022-11-23 11:59:28 CET; 10s ago
      Until: Wed 2022-11-23 11:59:28 CET; 10s ago
    Trigger: Thu 2022-11-24 04:00:00 CET; 16h left
   Triggers: ● db_dump.service

Nov 23 11:59:28 db.tp2.linux systemd[1]: Started Run service X.

[thomas@db ~]$ sudo systemctl list-timers
NEXT                        LEFT          LAST                        PASSED       UNIT                         ACTIVATES
Wed 2022-11-23 12:03:12 CET 2min 21s left Wed 2022-11-23 11:01:48 CET 59min ago    dnf-makecache.timer          dnf-makecache.service
Thu 2022-11-24 00:00:00 CET 11h left      Wed 2022-11-23 09:55:35 CET 2h 5min ago  logrotate.timer              logrotate.service
Thu 2022-11-24 04:00:00 CET 15h left      n/a                         n/a          db_dump.timer                db_dump.service
Thu 2022-11-24 10:10:48 CET 22h left      Wed 2022-11-23 10:10:48 CET 1h 50min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

4 timers listed.
```

➜ **Tester la restauration des données** sinon ça sert à rien :)

**Pour récupérer les données comment faire ?**
- Première étape récupérer la dernière sauvegarde qu'on a fait avec le service :
```
[thomas@db db_dumps]$ ls -lt /srv/db_dumps/ | head -2
total 304
-rw-r--r--. 1 db_dumps db_dumps  52285 Nov 23 11:56 nextcloud_221123115607.sql.gz
```

- Deuxième étape on unzip le fichier :
```
[thomas@db db_dumps]$ sudo gzip -d /srv/db_dumps/nextcloud_221123115607.sql.gz
```

- Ensuite on se connecte à la base de données en local :
```
[thomas@db db_dumps]$ mysql -u root -p nextcloud < /srv/db_dumps/db_nextcloud_221123115607.sql
```

- Enfin on fait un dump sur notre base de donnée avec le fichier de la backup qu'on a unzip :
```
[thomas@db db_dumps]$ mysql -u dump -p nextcloud < /srv/db_dumps/db_nextcloud_221123115607.sql
```

**Et voila on a maintenant restaurer les données sur notre base de données !**
