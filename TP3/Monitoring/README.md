# Module 5 : Monitoring

Dans ce sujet on va installer un outil plutôt clé en main pour mettre en place un monitoring simple de nos machines.

L'outil qu'on va utiliser est Netdata.

Installez-le sur `web.tp2.linux` et `db.tp2.linux`.

➜ **Configurer Netdata pour qu'il vous envoie des alertes dans un salon Discord.**

➜ **Vérifier que les alertes fonctionnent** en surchargeant volontairement la machine par exemple (effectuez des *stress tests* de RAM et CPU, ou remplissez le disque volontairement par exemple)

**Installation de NETDATA :**

```
[thomas@web ~]$ sudo dnf install epel-release -y
Last metadata expiration check: 0:16:04 ago on Mon 21 Nov 2022 04:49:56 PM CET.
Package epel-release-9-4.el9.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

```
[thomas@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --disable-telemetry

--2022-07-21 21:22:41--  https://my-netdata.io/kickstart.sh
Resolving my-netdata.io (my-netdata.io)... 188.114.97.3, 188.114.96.3, 2a06:98c1:3121::3, ...
Connecting to my-netdata.io (my-netdata.io)|188.114.97.3|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [application/octet-stream]
Saving to: ‘/tmp/netdata-kickstart.sh’

/tmp/netdata-kickstart.sh                           [ <=>                                                                                                 ]  75.12K  --.-KB/s    in 0.001s

2022-07-21 21:22:41 (87.6 MB/s) - ‘/tmp/netdata-kickstart.sh’ saved [76921]

 --- Using /tmp/netdata-kickstart-nGisWsmj1w as a temporary directory. ---
 --- Checking for existing installations of Netdata... ---
 --- No existing installations of netdata found, assuming this is a fresh install. ---
 --- Attempting to install using native packages... ---
 --- Checking for availability of repository configuration package. ---

.........
..........
.............

Thu Jul 21 09:24:22 PM CEST 2022 : INFO: netdata-updater.sh:  Auto-updating has been ENABLED through cron, updater script linked to /etc/cron.daily/netdata-updater\n
Thu Jul 21 09:24:22 PM CEST 2022 : INFO: netdata-updater.sh:  If the update process fails and you have email notifications set up correctly for cron on this system, you should receive an email notification of the failure.
Thu Jul 21 09:24:22 PM CEST 2022 : INFO: netdata-updater.sh:  Successful updates will not send an email.
Successfully installed the Netdata Agent.

Official documentation can be found online at https://learn.netdata.cloud/docs/.

Looking to monitor all of your infrastructure with Netdata? Check out Netdata Cloud at https://app.netdata.cloud.

Join our community and connect with us on:
  - GitHub: https://github.com/netdata/netdata/discussions
  - Discord: https://discord.gg/5ygS846fR6
  - Our community forums: https://community.netdata.cloud/
```

**Maintenant on peut lancer Netdata :**
```
[thomas@web ~]$ sudo systemctl start netdata
[thomas@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-21 17:07:55 CET; 3min 28s ago
   Main PID: 2113 (netdata)
      Tasks: 60 (limit: 5905)
     Memory: 82.7M
        CPU: 6.786s
     CGroup: /system.slice/netdata.service
             ├─2113 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─2115 /usr/sbin/netdata --special-spawn-server
             ├─2294 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
             ├─2298 /usr/libexec/netdata/plugins.d/apps.plugin 1
             └─2301 /usr/libexec/netdata/plugins.d/go.d.plugin 1
```

**On ouvre par la suite le port de Netdata sur le firewall :**
```
[thomas@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[thomas@web ~]$ sudo firewall-cmd --reload
success
[thomas@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services:
  ports: 22/tcp 80/tcp 19999/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

**Ensuite pour rendre la chose plus simple on va aller dans le fichier host et on va mettre un nom pour acceder a netdata :**
```
# localhost name resolution is handled within DNS itself.
#	127.0.0.1       localhost
#	::1             localhost
10.5.1.11       web.tp5.linux
10.102.1.13	web.tp2.linux
10.102.1.11	web.tp2.linux.netdata
```

**Et voila on a accès à Netdata pour tout surveiller**

![Netdata](./Netdata.PNG)


**Pour aller plus loin on va mettre en place une alerte sur Discord pour nous prévenir quand nos machines ont un problèmes**

**Pour ca on va changer le fichier de conf de netdata et on y ajoute un Webhook qu'on a crée sur discord :**
```
[thomas@web ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1044290927767859240/b8bO7Mf9s95o5dpd5DOg-qgYYqFgm3jJ54VV_AranlImaqrEL-J-fi0K3p22TI1h8c6s"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):

DEFAULT_RECIPIENT_DISCORD="général"
```

**Pour tester si les alarmes marchent vraiment j'utilise un stress test pour surcharger mon cpu grace à la commande :**

```
[thomas@web ~]$ stress --cpu 8 --io 4 --vm 2 --vm-bytes 128M --timeout 600s
stress: info: [4601] dispatching hogs: 8 cpu, 4 io, 2 vm, 0 hdd
```

Et on peut voir que Netdata m'a envoyé une notification sur Discord pour m'avertir que mon CPU était surchargé :

![alertes_netdata](./alertes_netdata.PNG)


**Et bien sur on installe netdata et on le configure sur toutes nos machines que ce soit DB ou DB_replication ou encore proxy, pour etre alerté de tout problèmes sur nos machines**
